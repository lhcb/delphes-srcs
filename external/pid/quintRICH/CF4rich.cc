#include "CF4rich.h"

#include <iostream>
#include <math.h>

using namespace std;


CF4rich::CF4rich(double l, double eL, double eH, double ps, double dpp, double con)
{
  length    = l;
  etaLow    = eL;
  etaHigh   = eH;
  padsize   = ps;
  dp_over_p = dpp;
  constant  = con;

  char nameit[500];
  sprintf(nameit,"CF4 Rich L=%d pad=%d mm",int(length),int(10*padsize));
  myName = nameit;

  //  Initialize a few constants:  Wikipedia...should be redone in a better manner...
  c       = 0.0299792458;  // cm/picosecond
  mPion   = 0.13957018;    //GeV/c^2
  mKaon   = 0.493677;      //GeV/c^2
  mProton = 0.93827208816; //GeV/c^2

  n = 1.000557873;     // Fit to test beam data...literature says 1.00056
  dispersion = 0.025;  // 2.5% based upon Monte Carlo of known CF4 dispersion.

}

double CF4rich::numSigma(double eta, double p, PID::type PID)
{
  //  The strategy is the following:
  //  a:  decide which is the narrow (low mass) peak and wide (high mass) peak.
  //  b:  calculate the radius center and width of the wide peak.
  //  c:  calculate the center of the narrow peak.
  //  d:  express the difference in centers as numbers of wide peak widths.

  if (valid(eta,p))
    {
      double mNarrow = mPion;
      double mWide   = mKaon;
      if (PID == k_p)
	{
	  mNarrow = mKaon;
	  mWide   = mProton;
	}
      
      double rWide = radius(p, mWide);
      double wWide = width (p, mWide);
      
      double rNarrow = radius(p, mNarrow);
      double wNarrow = width (p, mNarrow);
      
      return (rNarrow - rWide)/wWide;
    }

  else
    {
      //cout << "CF4rich.C:  Invalid (eta,p) for this detector." <<endl;
      return -1.0;
    }
      
}

double CF4rich::maxP(double eta, double numRequired, PID::type PID)
{
  //  This is an inefficient, but close to correct calculation.
  //  We start at p = 200 GeV and count down until the sigma is better than
  //  required.  We then interpolate linearly between the final two calculations.
  //
  //  We also apologize to all the CPUs that are offended to work so hard.
  //

  double num1 = numSigma(eta, 200, PID);
  if (num1 > numRequired) return 200;

  for (double p=199; p > 0; p -= 1.0)
    {
      if (valid(eta,p))
	{
	  double num2 = numSigma(eta, p, PID);
	  if (num2 > numRequired)
	    {
	      return p + (numRequired - num2)/(num1 - num2);
	    }
	  num1 = num2;
	}
      else
	{
	  cout << "CF4rich.C:  Unable to calculate maxP." <<endl;
	  return 0.0;
	}
      
    }

  return 0;
}

double CF4rich::radius(double p, double m)
{
  double p2_m2 = p*p/(m*m);
  double beta  = sqrt(p2_m2/(1+p2_m2));

  double thetac = acos(1/(n*beta));
  
  return (length * sin(thetac));
}

double CF4rich::width(double p, double m)
{
  //  This assumes that the width is comprised of multiple terms:
  //
  //  a:  Dispersion as given by an independent Monte Carlo Calculation.
  //  b:  Reconstructed Photo-electron Statistics (fit to the measurement from test beam).
  //  c:  pad size over sqrt(12)
  //  d:  momentum uncertainty
  //  e:  constant term parameterized from the test beam
  //

  //  Calculate the dispersion in radius for each photo-electron...
  double r = radius(p, m);
  double disp = dispersion * radius(p, m);

  //  The photon yield is a constant times Length * sin^2(theta_cherenkov)..
  double p2_m2   = p*p/(m*m);
  double beta    = sqrt(p2_m2/(1+p2_m2));
  double thetac  = acos(1/(n*beta));
  double Lsin2th = length * (sin(thetac) * sin(thetac));
  double Nphoton = 88.24 * Lsin2th;  // 88.24 is a fit to test beam data...

  //  Calculate dR due to momentum uncertainty.
  //
  double mp2   = (m/p)*(m/p);
  double dRnum = length * dp_over_p * mp2;
  double dRden = sqrt( n*n*n*n - n*n*(1+mp2) );
  double dR   = dRnum/dRden;

  // Calculate the individual contributions 
  double w1 = disp/sqrt(Nphoton);
  double w2 = padsize/sqrt(12*Nphoton);
  double w3 = dR;
  double w4 = constant/10000.0;  // Constant term from test beam data...

  if (false)//true)
    {
      cout << "  p: " << p;
      cout << "  m: " << m;
      cout << "  r: " << r;
      cout << "  N: " << Nphoton;
      cout << "  w1: " << w1;
      cout << "  w2: " << w2;
      cout << "  w3: " << w3;
      cout << "  w4: " << w4;
      cout << endl;
    }


  double width = sqrt(w1*w1 + w2*w2 + w3*w3 + w4*w4);

  return width;
}

void CF4rich::description()
{
  //  Here one should describe what this detector is and what assumptions are 
  //  made in calculating the performance specifications.

  cout << "My name is \"" << myName << "\" and I am described as follows:" <<endl;
  cout << "    I am a CF4-vbased RICH." <<endl;
  cout << "    I extend from eta =  " << etaLow << " until eta = " << etaHigh << " ." <<endl;
  cout << "    I have a radiator length of L= " << length << " cm." <<endl;
  cout << "    I have a pad diameter of " << padsize*10 << " mm." <<endl;
  cout << "    My calculations have assumed finite momentum resolution dp/p= " << dp_over_p <<endl;
  cout << "    My response includes these factors:." <<endl;
  cout << "      --Dispersion calculated from published CF4 data." <<endl;
  cout << "      --Finite pad size." <<endl;
  cout << "      --Photon yield parameterized from test beam." <<endl;
  cout << "      --Index of refraction parameterized from test beam." <<endl;
  cout << "      --A constant-term of dR=245 microns parameterized from test beam." <<endl;
  cout << endl;

}


bool CF4rich::valid (double eta, double p)
{
  return (eta>etaLow && eta<etaHigh) ;
}

double CF4rich::minP(double eta, double numSigma, PID::type PID)
{
  //  For minimum momentum, we shall assume that if the 
  //  lower mass particle radiates we can distinguish.
  //
  //  To define "radiates" we choose to require a photon yield 
  //  of about three photons which is 1/3 of the maximum yield.
  //
  //

  double m = mPion;
  if (PID == k_p)
    {
      m = mKaon;
    }

  double Nphoton = 3.0;
  double Lsin2th = Nphoton/88.24;
  double thetac  = asin(sqrt(Lsin2th/length));
  double beta    = 1.0/(n*cos(thetac));
  double gamma   = 1.0/sqrt(1-beta*beta);
  double p       = m*gamma*beta;

  return p;
}

#include "mRICH.h"

#include "TFile.h"
#include "TMath.h"

using namespace std;


mRICH::mRICH(double trackResolution, double timePrecision, double pix, double etaMin, double etaMax)
{
	
  // In this arameterization we assume that the particle enters the mRICH perpendicular to its front face. More realistic case will be implemented in the subsequent updates.

  etaLow = etaMin;
  etaHigh = etaMax;

  fTrackResolution = trackResolution;
  fTimePrecision = timePrecision;
  pLow    = 3;
  pHigh   = 10.1;
  c       = 0.0299792458;  // cm/picosecond
  n       = 1.03; //Aerogel
  a       = pix;// pixel size 3.0; // mm -- one side
  f       = 152.4; //focal length mm =  6"
  N_gam   = 10;
  mPion   = 0.13957018;    //GeV/c^2
  mKaon   = 0.493677;      //GeV/c^2
  mProton = 0.93827208816; //GeV/c^2
  pi      = 3.14159;
  alpha   = 0.0072973525693; // hyperfine const
  L       = 3.0;//Aerogel block thickness in cm

  //===============
  th0 = 0.; //incidence angle in radians

  fpixMap = nullptr;

  char nameit[500];
  sprintf(nameit,"mRICH Pixel Size =%dx%d (mm^{2})",int(pix),int(pix));
  myName = nameit;

  fVerbose = false;

  ngamma_integrand = -1.0;

  //cout<<".................\t"<<Nsigma<<endl;
}

double mRICH::maxP(double eta,double nSigma, PID::type PID)
{
  for(double p=15; p>0.4; p-=0.01){
    if(numSigma(eta, p, PID) > nSigma) return p;
  }

  return 0;
}

double mRICH::numSigma(double eta, double p, PID::type PID)
{
  double Nsigma = -1.0;
  if (valid(eta,p))
    {
      //Angle difference 
      double dth = getAng(mPion,p) - getAng(mKaon,p);
      //Detector uncertainty 
      double sigTh = sqrt(TMath::Power(getdAng(mPion,p),2)+TMath::Power(getdAng(mKaon,p),2));
      //Global uncertainty
      double sigThTrk = sqrt(TMath::Power(getdAngTrk(mPion,p),2)+TMath::Power(getdAngTrk(mKaon,p),2));
      double sigThc = sqrt(TMath::Power(sigTh/sqrt(getNgamma(L,mKaon,p)),2)+TMath::Power(sigThTrk,2));
      //std::cout << std::scientific << "dth = " << dth << std::fixed << ", sigThc = " << sigThc << std::endl;
      Nsigma = TMath::Abs(dth/sigThc);
      // cout << "p = " << p << std::scientific << " NSigma = " << Nsigma << std::fixed << std::endl;
      
#if 0
      //From simulations
      if (fpixMap == nullptr) {
	ReadMap("mRICHresol.root");
      }
      Nsigma = TMath::Abs(fpixMap->GetBinContent(p,a));
#endif

      // cout<<getNgamma(L,mPion,p)<<"\t"<<getNgamma(L,mKaon,p)<<endl;
      

    }
  else
    {
      if (fVerbose)
	cout << "mRICH.C:  Invalid (eta) for this detector." <<endl;
    }
  return Nsigma;
}

//Angle exiting the Aerogel
double mRICH::getAng(double mass, double p)
{

  double beta = p/TMath::Sqrt(TMath::Power(p,2)+TMath::Power(mass,2));
  double thc = acos(1./n/beta);
  double th0p = asin(sin(th0)/n);
  double dthc = thc - th0p;
  double theta = asin(n*sin(dthc));

  return theta;
}

//Uncertainty due to detector effects
double mRICH::getdAng(double mass, double p)
{

  double beta = p/TMath::Sqrt(TMath::Power(p,2)+TMath::Power(mass,2));
  double thc = acos(1./n/beta);
  double th0p = asin(sin(th0)/n);
  double dthc = thc - th0p;
  double theta = asin(n*sin(dthc));

  double sig_ep = 0;//Emission point error
  double sig_chro = 0; //Chromatic dispersion error
  double sig_pix = a*TMath::Power(cos(theta),2)/f/TMath::Sqrt(12.);;

  double sigTh = TMath::Sqrt(TMath::Power(sig_ep,2)+TMath::Power(sig_chro,2)+TMath::Power(sig_pix,2));

  return sigTh;
}

//Uncertainty due to tracking resolution
double mRICH::getdAngTrk(double mass, double p)
{

  double beta = p/TMath::Sqrt(TMath::Power(p,2)+TMath::Power(mass,2));
  double thc = acos(1./n/beta);
  double th0p = asin(sin(th0)/n);
  double dthc = thc - th0p;
  double theta = asin(n*sin(dthc));

  double sig_trk = (cos(dthc)/cos(theta))*(cos(th0)/cos(th0p))*fTrackResolution;;

  return sig_trk;
}

//no. of gamms
double mRICH::getNgamma(double t, double mass, double p)
{
  int tot = 10000;
  double beta = p/TMath::Sqrt(TMath::Power(p,2)+TMath::Power(mass,2));
  double fact = 2.*pi*alpha*t*(1.-1./TMath::Power(n*beta,2));
  double T_lensWin = 0.92*0.92;
  double xmin = 300.e-7;
  double xmax = 650.e-7;
  double dx = (xmax-xmin)/tot;
  double sum = 0;
  if (ngamma_integrand < 0.0) {
    // compute this sum only once, if possible!
    for(int j=0; j<tot; j++){
      double x = xmin + j*dx + dx/2.;
      sum += T_QE(x)*T_Aer(t,x)/TMath::Power(x,2);
    }
    ngamma_integrand = sum;
  }
  //cout << "Integrand: " << ngamma_integrand << endl;
    
  //cout << "fact = " << fact << " T_lensWin = " << T_lensWin << " Integrand: " << ngamma_integrand << " dx = " << std::scientific << dx << std::fixed << endl;
  //cout << "product = " << std::scientific << fact*T_lensWin*ngamma_integrand*dx << std::fixed << endl;

  return fact*T_lensWin*ngamma_integrand*dx;
}

//Quantum efficiency
double mRICH::T_QE(double lam)
{
  return 0.34*exp(-1.*TMath::Power(lam-345.e-7,2)/(2.*TMath::Power(119.e-7,2)));
}

//Transmissions of the radiator block
double mRICH::T_Aer(double t, double lam)
{
  return 0.83*exp(-1.*t*56.29e-20/TMath::Power(lam,4));
}


//Read histogram from MC parametrization
void  mRICH::ReadMap(TString name){
  TFile* file = TFile::Open(name);
  fpixMap = (TH2F *) file->Get("h2pix");
}


void mRICH::description()
{
  //  Here one should describe what this detector is and what assumptions are 
  //  made in calculating the performance specifications.

  cout << "My name is \"" << myName << "\" and I am described as follows:" <<endl;
  cout << "    Momentum coverage for k/pi separation=  [" << 3.0 << " to " << 10.0 <<" (GeV/c) ]"<<endl;
  cout << "    Since mRICH detector system consists of array of identical shoebox-like modules, in principle, " << endl;
  cout << "    the mRICH pid performance is invariant relative to the tracking polar angle but the momentum of the " << endl;
  cout << "    charge particle and the entrance point location on the front face of a given mRICH module." << endl;
  cout << "    Assumed time precision = " << fTimePrecision << " ns" <<endl;
  cout << "    Assumed track resolution = "<< fTrackResolution << " mrad" <<endl;
  //cout << "    Assumed quantum efficiency of the photosensors = "<< fSensorQMefficiency << "%" <<endl;
  cout << endl;
  
}
